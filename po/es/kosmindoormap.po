# Spanish translations for kosmindoormap.po package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kosmindoormap package.
#
# Automatically generated, 2020.
# Eloy Cuadra <ecuadra@eloihr.net>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kosmindoormap\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-02 00:45+0000\n"
"PO-Revision-Date: 2023-07-03 04:03+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: app/main.cpp:50
#, kde-format
msgid "KDE OSM Indoor Map"
msgstr "Mapas interiores OSM de KDE"

#: map-quick/floorlevelchangemodel.cpp:218
#, kde-format
msgid "Elevator"
msgstr "Ascensor"

#: map-quick/floorlevelchangemodel.cpp:225
#, kde-format
msgid "Staircase"
msgstr "Escalera"

#: map-quick/osmelementinformationmodel.cpp:21
#, kde-format
msgid "%1m"
msgstr "%1 m"

#: map-quick/osmelementinformationmodel.cpp:24
#: map-quick/osmelementinformationmodel.cpp:26
#, kde-format
msgid "%1km"
msgstr "%1 km"

#: map-quick/osmelementinformationmodel.cpp:135
#, kde-format
msgid "Wikipedia"
msgstr "Wikipedia"

#: map-quick/osmelementinformationmodel.cpp:431
#, kde-format
msgid "Opening Hours"
msgstr "Horario de apertura"

#: map-quick/osmelementinformationmodel.cpp:432
#, kde-format
msgid "Contact"
msgstr "Contacto"

#: map-quick/osmelementinformationmodel.cpp:433
#, kde-format
msgid "Payment"
msgstr "Pago"

#: map-quick/osmelementinformationmodel.cpp:434
#, kde-format
msgid "Toilets"
msgstr "Lavabos"

#: map-quick/osmelementinformationmodel.cpp:435
#, kde-format
msgid "Accessibility"
msgstr "Accesibilidad"

#: map-quick/osmelementinformationmodel.cpp:436
#, kde-format
msgid "Parking"
msgstr "Aparcamiento"

#: map-quick/osmelementinformationmodel.cpp:437
#, kde-format
msgid "Operator"
msgstr "Operador"

#: map-quick/osmelementinformationmodel.cpp:463
#, kde-format
msgid "Formerly"
msgstr "Anteriormente"

#: map-quick/osmelementinformationmodel.cpp:464
#, kde-format
msgid "Description"
msgstr "Descripción"

#: map-quick/osmelementinformationmodel.cpp:465
#, kde-format
msgid "Routes"
msgstr "Rutas"

#: map-quick/osmelementinformationmodel.cpp:466
#, kde-format
msgid "Cuisine"
msgstr "Cocina"

#: map-quick/osmelementinformationmodel.cpp:467
#, kde-format
msgid "Diet"
msgstr "Dietética"

#: map-quick/osmelementinformationmodel.cpp:468
#, kde-format
msgid "Takeaway"
msgstr "Para llevar"

#: map-quick/osmelementinformationmodel.cpp:469
#, kde-format
msgctxt "electrical power socket"
msgid "Socket"
msgstr "Toma de corriente"

#: map-quick/osmelementinformationmodel.cpp:471
#, kde-format
msgid "Available vehicles"
msgstr "Vehículos disponibles"

#: map-quick/osmelementinformationmodel.cpp:472
#, kde-format
msgid "Fee"
msgstr "Tarifa"

#: map-quick/osmelementinformationmodel.cpp:473
#, kde-format
msgid "Authentication"
msgstr "Autenticación"

#: map-quick/osmelementinformationmodel.cpp:474
#, kde-format
msgid "Bicycle parking"
msgstr "Aparcamiento de bicicletas"

#: map-quick/osmelementinformationmodel.cpp:475
#, kde-format
msgid "Capacity"
msgstr "Capacidad"

#: map-quick/osmelementinformationmodel.cpp:476
#, kde-format
msgid "Disabled parking spaces"
msgstr "Plazas de aparcamiento para personas con discapacidad"

#: map-quick/osmelementinformationmodel.cpp:477
#, kde-format
msgid "Women parking spaces"
msgstr "Plazas de aparcamiento para mujeres"

#: map-quick/osmelementinformationmodel.cpp:478
#, kde-format
msgid "Parent parking spaces"
msgstr "Plazas de aparcamiento para padres"

#: map-quick/osmelementinformationmodel.cpp:479
#, kde-format
msgid "Parking spaces for charging"
msgstr "Plazas de aparcamiento para recarga"

#: map-quick/osmelementinformationmodel.cpp:480
#, kde-format
msgid "Maximum stay"
msgstr "Estancia máxima"

#: map-quick/osmelementinformationmodel.cpp:481
#, kde-format
msgid "Diaper changing table"
msgstr "Cambiador de pañales"

#: map-quick/osmelementinformationmodel.cpp:482
#, kde-format
msgid "Gender"
msgstr "Género"

#: map-quick/osmelementinformationmodel.cpp:484
#, kde-format
msgid "Address"
msgstr "Dirección"

#: map-quick/osmelementinformationmodel.cpp:485
#, kde-format
msgid "Phone"
msgstr "Teléfono"

#: map-quick/osmelementinformationmodel.cpp:486
#, kde-format
msgid "Email"
msgstr "Correo electrónico"

#: map-quick/osmelementinformationmodel.cpp:487
#, kde-format
msgid "Website"
msgstr "Sitio web"

#: map-quick/osmelementinformationmodel.cpp:488
#, kde-format
msgid "Cash"
msgstr "Efectivo"

#: map-quick/osmelementinformationmodel.cpp:489
#, kde-format
msgid "Digital"
msgstr "Digital"

#: map-quick/osmelementinformationmodel.cpp:490
#, kde-format
msgid "Debit cards"
msgstr "Tarjetas de débito"

#: map-quick/osmelementinformationmodel.cpp:491
#, kde-format
msgid "Credit cards"
msgstr "Tarjetas de crédito"

#: map-quick/osmelementinformationmodel.cpp:492
#, kde-format
msgid "Stored value cards"
msgstr "Tarjetas de valor guardado"

#: map-quick/osmelementinformationmodel.cpp:493
#, kde-format
msgid "Wheelchair access"
msgstr "Acceso para sillas de ruedas"

#: map-quick/osmelementinformationmodel.cpp:494
#, kde-format
msgid "Wheelchair lift"
msgstr "Ascensor para sillas de ruedas"

#: map-quick/osmelementinformationmodel.cpp:495
#, kde-format
msgid "Central key"
msgstr "Cerradura central"

#: map-quick/osmelementinformationmodel.cpp:496
#, kde-format
msgid "Speech output"
msgstr "Salida de voz"

#: map-quick/osmelementinformationmodel.cpp:497
#, kde-format
msgid "Tactile writing"
msgstr "Escritura táctil"

#: map-quick/osmelementinformationmodel.cpp:499
#, kde-format
msgctxt "transport network"
msgid "Network"
msgstr "Red"

#: map-quick/osmelementinformationmodel.cpp:501
#, kde-format
msgctxt "remaining travel range of a battery powered vehicle"
msgid "Remaining range"
msgstr "Alcance restante"

#: map-quick/osmelementinformationmodel.cpp:553
#, kde-format
msgctxt "local name (transliterated name)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: map-quick/osmelementinformationmodel.cpp:623
#, kde-format
msgid "only %1"
msgstr "solo %1"

#: map-quick/osmelementinformationmodel.cpp:625
#, kde-format
msgid "no %1"
msgstr "sin %1"

#: map-quick/osmelementinformationmodel.cpp:650
#, kde-format
msgctxt "electrical current/Ampere value"
msgid "%1 A"
msgstr "%1 A"

#: map-quick/osmelementinformationmodel.cpp:658
#, kde-format
msgctxt "electrical power/kilowatt value"
msgid "%1 kW"
msgstr "%1 kW"

#: map-quick/osmelementinformationmodel.cpp:689
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: map-quick/osmelementinformationmodel.cpp:757
#: map-quick/osmelementinformationmodel.cpp:931
#, kde-format
msgid "yes"
msgstr "sí"

#: map-quick/osmelementinformationmodel.cpp:760
#, kde-format
msgctxt "payment option"
msgid "coins only"
msgstr "solo en moneda"

#: map-quick/osmelementinformationmodel.cpp:763
#, kde-format
msgctxt "payment option"
msgid "notes only"
msgstr "solo en billetes"

#: map-quick/osmelementinformationmodel.cpp:765
#: map-quick/osmelementinformationmodel.cpp:816
#: map-quick/osmelementinformationmodel.cpp:934
#, kde-format
msgid "no"
msgstr "no"

#: map-quick/osmelementinformationmodel_data.cpp:22
msgctxt "OSM::amenity/shop"
msgid "Alcohol"
msgstr "Bebidas alcohólicas"

#: map-quick/osmelementinformationmodel_data.cpp:23
msgctxt "OSM::amenity/shop"
msgid "Apartment"
msgstr "Apartamento"

#: map-quick/osmelementinformationmodel_data.cpp:24
msgctxt "OSM::amenity/shop"
msgid "Arts Center"
msgstr "Centro de artes"

#: map-quick/osmelementinformationmodel_data.cpp:25
msgctxt "OSM::amenity/shop"
msgid "Artwork"
msgstr "Obra de arte"

#: map-quick/osmelementinformationmodel_data.cpp:26
msgctxt "OSM::amenity/shop"
msgid "ATM"
msgstr "Cajero automático"

#: map-quick/osmelementinformationmodel_data.cpp:27
msgctxt "OSM::amenity/shop"
msgid "Attraction"
msgstr "Atracción"

#: map-quick/osmelementinformationmodel_data.cpp:28
msgctxt "OSM::amenity/shop"
msgid "Bag"
msgstr "Bolsos"

#: map-quick/osmelementinformationmodel_data.cpp:29
msgctxt "OSM::amenity/shop"
msgid "Bakery"
msgstr "Panadería"

#: map-quick/osmelementinformationmodel_data.cpp:30
msgctxt "OSM::amenity/shop"
msgid "Bank"
msgstr "Banco"

#: map-quick/osmelementinformationmodel_data.cpp:31
msgctxt "OSM::amenity/shop"
msgid "Bar"
msgstr "Bar"

#: map-quick/osmelementinformationmodel_data.cpp:32
msgctxt "OSM::amenity/shop"
msgid "Beauty"
msgstr "Belleza"

#: map-quick/osmelementinformationmodel_data.cpp:33
msgctxt "OSM::amenity/shop"
msgid "Bed"
msgstr "Cama"

#: map-quick/osmelementinformationmodel_data.cpp:34
msgctxt "OSM::amenity"
msgid "Bench"
msgstr "Banco"

#: map-quick/osmelementinformationmodel_data.cpp:35
msgctxt "OSM::amenity/shop"
msgid "Beverages"
msgstr "Bebidas"

#: map-quick/osmelementinformationmodel_data.cpp:36
msgctxt "OSM::amenity/shop"
msgid "Bicycle"
msgstr "Bicicleta"

#: map-quick/osmelementinformationmodel_data.cpp:37
msgctxt "OSM::amenity/shop"
msgid "Bicycle Parking"
msgstr "Aparcamiento para bicicletas"

#: map-quick/osmelementinformationmodel_data.cpp:38
msgctxt "OSM::amenity/shop"
msgid "Bicycle Rental"
msgstr "Alquiler de bicicletas"

#: map-quick/osmelementinformationmodel_data.cpp:39
msgctxt "OSM::amenity/shop"
msgid "Books"
msgstr "Libros"

#: map-quick/osmelementinformationmodel_data.cpp:40
msgctxt "OSM::amenity/shop"
msgid "Boutique"
msgstr "Boutique"

#: map-quick/osmelementinformationmodel_data.cpp:41
msgctxt "OSM::amenity/shop"
msgid "Bureau de Change"
msgstr "Casa de cambio"

#: map-quick/osmelementinformationmodel_data.cpp:42
msgctxt "OSM::amenity/shop"
msgid "Butcher"
msgstr "Carnicería"

#: map-quick/osmelementinformationmodel_data.cpp:43
msgctxt "OSM::amenity/shop"
msgid "Cafe"
msgstr "Café"

#: map-quick/osmelementinformationmodel_data.cpp:44
msgctxt "OSM::amenity/shop"
msgid "Car"
msgstr "Automóvil"

#: map-quick/osmelementinformationmodel_data.cpp:45
msgctxt "OSM::amenity/shop"
msgid "Car Rental"
msgstr "Alquiler de vehículos"

#: map-quick/osmelementinformationmodel_data.cpp:46
msgctxt "OSM::amenity/shop"
msgid "Car Sharing"
msgstr "Vehículos compartidos"

#: map-quick/osmelementinformationmodel_data.cpp:47
msgctxt "OSM::amenity/shop"
msgid "Charging Station"
msgstr "Estación de recarga"

#: map-quick/osmelementinformationmodel_data.cpp:48
msgctxt "OSM::amenity/shop"
msgid "Chemist"
msgstr "Farmacia"

#: map-quick/osmelementinformationmodel_data.cpp:49
msgctxt "OSM::amenity/shop"
msgid "Chocolate"
msgstr "Chocolate"

#: map-quick/osmelementinformationmodel_data.cpp:50
msgctxt "OSM::amenity/shop"
msgid "Cinema"
msgstr "Cine"

#: map-quick/osmelementinformationmodel_data.cpp:51
msgctxt "OSM::historic"
msgid "Citywall"
msgstr "Muralla"

#: map-quick/osmelementinformationmodel_data.cpp:52
#: map-quick/osmelementinformationmodel_data.cpp:53
msgctxt "OSM::room"
msgid "Classroom"
msgstr "Aula"

#: map-quick/osmelementinformationmodel_data.cpp:54
msgctxt "OSM::amenity/shop"
msgid "Clothes"
msgstr "Ropa"

#: map-quick/osmelementinformationmodel_data.cpp:55
msgctxt "OSM::amenity/shop"
msgid "Coffee"
msgstr "Café"

#: map-quick/osmelementinformationmodel_data.cpp:56
msgctxt "OSM::amenity"
msgid "Community Center"
msgstr "Centro comunitario"

#: map-quick/osmelementinformationmodel_data.cpp:57
msgctxt "OSM::amenity/shop"
msgid "Computer"
msgstr "Informática"

#: map-quick/osmelementinformationmodel_data.cpp:58
msgctxt "OSM::amenity/shop"
msgid "Confectionery"
msgstr "Confitería"

#: map-quick/osmelementinformationmodel_data.cpp:59
msgctxt "OSM::amenity/shop"
msgid "Convenience Store"
msgstr "Tienda de conveniencia"

#: map-quick/osmelementinformationmodel_data.cpp:60
msgctxt "OSM::amenity/shop"
msgid "Copy Shop"
msgstr "Copistería"

#: map-quick/osmelementinformationmodel_data.cpp:61
msgctxt "OSM::amenity/shop"
msgid "Cosmetics"
msgstr "Cosméticos"

#: map-quick/osmelementinformationmodel_data.cpp:62
msgctxt "OSM::amenity/shop"
msgid "Court House"
msgstr "Juzgado"

#: map-quick/osmelementinformationmodel_data.cpp:63
msgctxt "OSM::amenity/shop"
msgid "Deli"
msgstr "Delicatessen"

#: map-quick/osmelementinformationmodel_data.cpp:64
msgctxt "OSM::amenity/shop"
msgid "Department Store"
msgstr "Gran almacén"

#: map-quick/osmelementinformationmodel_data.cpp:65
msgctxt "OSM::amenity/shop"
msgid "Doctor"
msgstr "Doctor"

#: map-quick/osmelementinformationmodel_data.cpp:66
msgctxt "OSM::amenity/shop"
msgid "Hardware Store"
msgstr "Ferretería"

#: map-quick/osmelementinformationmodel_data.cpp:67
msgctxt "OSM::amenity/shop"
msgid "Drinking Water"
msgstr "Agua potable"

#: map-quick/osmelementinformationmodel_data.cpp:68
msgctxt "OSM::amenity/shop"
msgid "Dry Cleaning"
msgstr "Limpieza en seco"

#: map-quick/osmelementinformationmodel_data.cpp:69
msgctxt "OSM::amenity/shop"
msgid "Electronics"
msgstr "Electrónica"

#: map-quick/osmelementinformationmodel_data.cpp:70
msgctxt "OSM::amenity"
msgid "Events Venue"
msgstr "Lugar de eventos"

#: map-quick/osmelementinformationmodel_data.cpp:71
msgctxt "OSM::amenity/shop"
msgid "Fashion"
msgstr "Moda"

#: map-quick/osmelementinformationmodel_data.cpp:72
msgctxt "OSM::amenity/shop"
msgid "Fast Food"
msgstr "Comida rápida"

#: map-quick/osmelementinformationmodel_data.cpp:73
msgctxt "OSM::amenity/shop"
msgid "Ferry Terminal"
msgstr "Terminal de ferry"

#: map-quick/osmelementinformationmodel_data.cpp:74
msgctxt "OSM::amenity/shop"
msgid "Florist"
msgstr "Floristería"

#: map-quick/osmelementinformationmodel_data.cpp:75
msgctxt "OSM::amenity/shop"
msgid "Food Court"
msgstr "Zona de restaurantes"

#: map-quick/osmelementinformationmodel_data.cpp:76
msgctxt "OSM::amenity/shop"
msgid "Fountain"
msgstr "Fuente"

#: map-quick/osmelementinformationmodel_data.cpp:77
msgctxt "OSM::amenity/shop"
msgid "Furniture"
msgstr "Mobiliario"

#: map-quick/osmelementinformationmodel_data.cpp:78
msgctxt "OSM::amenity/shop"
msgid "Gallery"
msgstr "Galería"

#: map-quick/osmelementinformationmodel_data.cpp:79
msgctxt "OSM::amenity/shop"
msgid "Garden"
msgstr "Jardinería"

#: map-quick/osmelementinformationmodel_data.cpp:80
msgctxt "OSM::amenity/shop"
msgid "Garden Center"
msgstr "Vivero"

#: map-quick/osmelementinformationmodel_data.cpp:81
msgctxt "OSM::amenity/shop"
msgid "Gift Shop"
msgstr "Artículos de regalo"

#: map-quick/osmelementinformationmodel_data.cpp:82
msgctxt "OSM::office"
msgid "Government"
msgstr "Gobierno"

#: map-quick/osmelementinformationmodel_data.cpp:83
msgctxt "OSM::amenity/shop"
msgid "Greengrocer"
msgstr "Verduras"

#: map-quick/osmelementinformationmodel_data.cpp:84
msgctxt "OSM::amenity/shop"
msgid "Guest House"
msgstr "Pensión"

#: map-quick/osmelementinformationmodel_data.cpp:85
msgctxt "OSM::amenity/shop"
msgid "Hairdresser"
msgstr "Peluquería"

#: map-quick/osmelementinformationmodel_data.cpp:86
msgctxt "OSM::amenity/shop"
msgid "Hearing Aids"
msgstr "Audífonos"

#: map-quick/osmelementinformationmodel_data.cpp:87
msgctxt "OSM::amenity/shop"
msgid "Hospital"
msgstr "Hospital"

#: map-quick/osmelementinformationmodel_data.cpp:88
msgctxt "OSM::amenity/shop"
msgid "Hostel"
msgstr "Hostal"

#: map-quick/osmelementinformationmodel_data.cpp:89
msgctxt "OSM::amenity/shop"
msgid "Hotel"
msgstr "Hotel"

#: map-quick/osmelementinformationmodel_data.cpp:90
msgctxt "OSM::amenity/shop"
msgid "Houseware"
msgstr "Artículos del hogar"

#: map-quick/osmelementinformationmodel_data.cpp:91
msgctxt "OSM::amenity/shop"
msgid "Ice Cream"
msgstr "Helados"

#: map-quick/osmelementinformationmodel_data.cpp:92
msgctxt "OSM::amenity/shop"
msgid "Information"
msgstr "Información"

#: map-quick/osmelementinformationmodel_data.cpp:93
msgctxt "OSM::amenity/shop"
msgid "Interior Decoration"
msgstr "Interiorismo"

#: map-quick/osmelementinformationmodel_data.cpp:94
msgctxt "OSM::amenity/shop"
msgid "Internet Cafe"
msgstr "Cibercafé"

#: map-quick/osmelementinformationmodel_data.cpp:95
msgctxt "OSM::amenity/shop"
msgid "Jewelry"
msgstr "Joyería"

#: map-quick/osmelementinformationmodel_data.cpp:96
msgctxt "OSM::amenity/shop"
msgid "Kiosk"
msgstr "Kiosko"

#: map-quick/osmelementinformationmodel_data.cpp:97
msgctxt "OSM::room"
msgid "Kitchen"
msgstr "Cocina"

#: map-quick/osmelementinformationmodel_data.cpp:98
msgctxt "OSM::amenity/shop"
msgid "Laundry"
msgstr "Lavandería"

#: map-quick/osmelementinformationmodel_data.cpp:99
#: map-quick/osmelementinformationmodel_data.cpp:100
msgctxt "OSM::room"
msgid "Lecture Hall"
msgstr "Auditorio"

#: map-quick/osmelementinformationmodel_data.cpp:101
msgctxt "OSM::amenity/shop"
msgid "Library"
msgstr "Biblioteca"

#: map-quick/osmelementinformationmodel_data.cpp:102
#: map-quick/osmelementinformationmodel_data.cpp:106
msgctxt "OSM::amenity/shop"
msgid "Locker"
msgstr "Taquilla"

#: map-quick/osmelementinformationmodel_data.cpp:103
msgctxt "OSM::amenity/shop"
msgid "Locksmith"
msgstr "Cerrajero"

#: map-quick/osmelementinformationmodel_data.cpp:104
#: map-quick/osmelementinformationmodel_data.cpp:105
msgctxt "OSM::amenity/shop"
msgid "Lost & Found"
msgstr "Objetos perdidos"

#: map-quick/osmelementinformationmodel_data.cpp:107
msgctxt "OSM::amenity/shop"
msgid "Mall"
msgstr "Centro comercial"

#: map-quick/osmelementinformationmodel_data.cpp:108
msgctxt "OSM::amenity"
msgid "Marketplace"
msgstr "Mercado"

#: map-quick/osmelementinformationmodel_data.cpp:109
msgctxt "OSM::amenity/shop"
msgid "Medical Supply"
msgstr "Suministros médicos"

#: map-quick/osmelementinformationmodel_data.cpp:110
msgctxt "OSM::historic"
msgid "Memorial"
msgstr "Monumento conmemorativo"

#: map-quick/osmelementinformationmodel_data.cpp:111
msgctxt "OSM::amenity/shop"
msgid "Mobile Phone"
msgstr "Telefonía móvil"

#: map-quick/osmelementinformationmodel_data.cpp:112
msgctxt "OSM::amenity/shop"
msgid "Money Transfer"
msgstr "Envío de dinero"

#: map-quick/osmelementinformationmodel_data.cpp:113
msgctxt "OSM::historic"
msgid "Monument"
msgstr "Monumento"

#: map-quick/osmelementinformationmodel_data.cpp:114
msgctxt "OSM::amenity/shop"
msgid "Motorcycle Parking"
msgstr "Aparcamiento de motos"

#: map-quick/osmelementinformationmodel_data.cpp:115
msgctxt "OSM::amenity/shop"
msgid "Motorcycle Rental"
msgstr "Alquiler de motocicletas"

#: map-quick/osmelementinformationmodel_data.cpp:116
msgctxt "OSM::amenity/shop"
msgid "Museum"
msgstr "Museo"

#: map-quick/osmelementinformationmodel_data.cpp:117
msgctxt "OSM::amenity/shop"
msgid "Music"
msgstr "Música"

#: map-quick/osmelementinformationmodel_data.cpp:118
msgctxt "OSM::amenity/shop"
msgid "Musical Instruments"
msgstr "Instrumentos musicales"

#: map-quick/osmelementinformationmodel_data.cpp:119
msgctxt "OSM::amenity/shop"
msgid "Newsagent"
msgstr "Venta de prensa"

#: map-quick/osmelementinformationmodel_data.cpp:120
msgctxt "OSM::amenity/shop"
msgid "Office"
msgstr "Oficina"

#: map-quick/osmelementinformationmodel_data.cpp:121
msgctxt "OSM::amenity/shop"
msgid "Optician"
msgstr "Óptica"

#: map-quick/osmelementinformationmodel_data.cpp:122
msgctxt "OSM::amenity/shop"
msgid "Outdoor"
msgstr "Exterior"

#: map-quick/osmelementinformationmodel_data.cpp:123
msgctxt "OSM::amenity/shop"
msgid "Paint"
msgstr "Pinturas"

#: map-quick/osmelementinformationmodel_data.cpp:124
msgctxt "outdoor recreational area"
msgid "Park"
msgstr "Aparcamiento"

#: map-quick/osmelementinformationmodel_data.cpp:125
msgctxt "OSM::amenity/shop"
msgid "Parking"
msgstr "Aparcamiento"

#: map-quick/osmelementinformationmodel_data.cpp:126
msgctxt "OSM::amenity/shop"
msgid "Parking Tickets"
msgstr "Billetes de aparcamiento"

#: map-quick/osmelementinformationmodel_data.cpp:127
msgctxt "OSM::amenity/shop"
msgid "Pastry"
msgstr "Pastelería"

#: map-quick/osmelementinformationmodel_data.cpp:128
msgctxt "OSM::amenity/shop"
msgid "Perfumery"
msgstr "Perfumería"

#: map-quick/osmelementinformationmodel_data.cpp:129
msgctxt "OSM::amenity/shop"
msgid "Pet"
msgstr "Mascotas"

#: map-quick/osmelementinformationmodel_data.cpp:130
msgctxt "OSM::amenity/shop"
msgid "Pharmacy"
msgstr "Farmacia"

#: map-quick/osmelementinformationmodel_data.cpp:131
msgctxt "OSM::amenity/shop"
msgid "Photo"
msgstr "Fotografía"

#: map-quick/osmelementinformationmodel_data.cpp:132
msgctxt "OSM::amenity/shop"
msgid "Place of Worship"
msgstr "Lugar de culto"

#: map-quick/osmelementinformationmodel_data.cpp:133
msgid "Playground"
msgstr "Parque infantil"

#: map-quick/osmelementinformationmodel_data.cpp:134
msgctxt "OSM::amenity/shop"
msgid "Police"
msgstr "Policía"

#: map-quick/osmelementinformationmodel_data.cpp:135
msgctxt "OSM::amenity/shop"
msgid "Post Box"
msgstr "Buzón"

#: map-quick/osmelementinformationmodel_data.cpp:136
msgctxt "OSM::amenity/shop"
msgid "Post Office"
msgstr "Oficina de correos"

#: map-quick/osmelementinformationmodel_data.cpp:137
msgctxt "OSM::amenity/shop"
msgid "Pub"
msgstr "Pub"

#: map-quick/osmelementinformationmodel_data.cpp:138
msgctxt "OSM::amenity/shop"
msgid "Public Transport Tickets"
msgstr "Billetes de transporte público"

#: map-quick/osmelementinformationmodel_data.cpp:139
#: map-quick/osmelementinformationmodel_data.cpp:165
msgctxt "OSM::building"
msgid "Train Station"
msgstr "Estación de tren"

#: map-quick/osmelementinformationmodel_data.cpp:140
msgctxt "OSM::amenity/shop"
msgid "Recycling"
msgstr "Reciclaje"

#: map-quick/osmelementinformationmodel_data.cpp:141
msgctxt "OSM::amenity"
msgid "Research Institute"
msgstr "Instituto de investigación"

#: map-quick/osmelementinformationmodel_data.cpp:142
msgctxt "OSM::amenity/shop"
msgid "Restaurant"
msgstr "Restaurante"

#: map-quick/osmelementinformationmodel_data.cpp:143
msgctxt "OSM::amenity/shop"
msgid "School"
msgstr "Escuela"

#: map-quick/osmelementinformationmodel_data.cpp:144
msgctxt "OSM::amenity/shop"
msgid "Kick Scooter Rental"
msgstr "Alquiler de patinetes"

#: map-quick/osmelementinformationmodel_data.cpp:145
msgctxt "OSM::amenity/shop"
msgid "Seafood"
msgstr "Marisco"

#: map-quick/osmelementinformationmodel_data.cpp:146
msgctxt "OSM::amenity/shop"
msgid "Shoes"
msgstr "Zapatos"

#: map-quick/osmelementinformationmodel_data.cpp:147
msgctxt "OSM::amenity/shop"
msgid "Shop"
msgstr "Tienda"

#: map-quick/osmelementinformationmodel_data.cpp:148
msgctxt "OSM::amenity/shop"
msgid "Social Facility"
msgstr "Servicio social"

#: map-quick/osmelementinformationmodel_data.cpp:149
msgctxt "OSM::amenity/shop"
msgid "Souvenirs"
msgstr "Recuerdos"

#: map-quick/osmelementinformationmodel_data.cpp:150
msgctxt "OSM::amenity/shop"
msgid "Sports"
msgstr "Deportes"

#: map-quick/osmelementinformationmodel_data.cpp:151
msgctxt "OSM::amenity"
msgid "Sports Center"
msgstr "Centro deportivo"

#: map-quick/osmelementinformationmodel_data.cpp:152
msgctxt "OSM::amenity"
msgid "Sports Hall"
msgstr "Polideportivo"

#: map-quick/osmelementinformationmodel_data.cpp:153
msgctxt "OSM::amenity/shop"
msgid "Stationery"
msgstr "Papelería"

#: map-quick/osmelementinformationmodel_data.cpp:154
msgctxt "OSM::amenity/shop"
msgid "Supermarket"
msgstr "Supermercado"

#: map-quick/osmelementinformationmodel_data.cpp:155
msgctxt "OSM::amenity/shop"
msgid "Tailor"
msgstr "Sastre"

#: map-quick/osmelementinformationmodel_data.cpp:156
msgctxt "OSM::amenity/shop"
msgid "Tattoo"
msgstr "Tatuajes"

#: map-quick/osmelementinformationmodel_data.cpp:157
msgctxt "OSM::amenity/shop"
msgid "Taxi"
msgstr "Taxi"

#: map-quick/osmelementinformationmodel_data.cpp:158
msgctxt "OSM::amenity/shop"
msgid "Tea"
msgstr "Té"

#: map-quick/osmelementinformationmodel_data.cpp:159
msgctxt "OSM::amenity/shop"
msgid "Theatre"
msgstr "Teatro"

#: map-quick/osmelementinformationmodel_data.cpp:160
msgctxt "OSM::amenity/shop"
msgid "Tickets"
msgstr "Billetes"

#: map-quick/osmelementinformationmodel_data.cpp:161
msgctxt "OSM::amenity/shop"
msgid "Tobacco"
msgstr "Tabaco"

#: map-quick/osmelementinformationmodel_data.cpp:162
msgctxt "OSM::amenity/shop"
msgid "Toilets"
msgstr "Cuartos de baño"

#: map-quick/osmelementinformationmodel_data.cpp:163
msgctxt "OSM::amenity/shop"
msgid "Town Hall"
msgstr "Ayuntamiento"

#: map-quick/osmelementinformationmodel_data.cpp:164
msgctxt "OSM::amenity/shop"
msgid "Toys"
msgstr "Juguetes"

#: map-quick/osmelementinformationmodel_data.cpp:166
#: map-quick/osmelementinformationmodel_data.cpp:167
msgctxt "OSM::amenity/shop"
msgid "Travel Agency"
msgstr "Agencia de viajes"

#: map-quick/osmelementinformationmodel_data.cpp:168
msgctxt "OSM::amenity/shop"
msgid "University"
msgstr "Universidad"

#: map-quick/osmelementinformationmodel_data.cpp:169
msgctxt "OSM::amenity/shop"
msgid "Variety Store"
msgstr "Bazar"

#: map-quick/osmelementinformationmodel_data.cpp:170
msgctxt "OSM::amenity/shop"
msgid "Video Games"
msgstr "Videojuegos"

#: map-quick/osmelementinformationmodel_data.cpp:171
#: map-quick/osmelementinformationmodel_data.cpp:172
#: map-quick/osmelementinformationmodel_data.cpp:173
msgctxt "OSM::amenity/shop"
msgid "Waiting Area"
msgstr "Área de espera"

#: map-quick/osmelementinformationmodel_data.cpp:174
msgctxt "OSM::amenity"
msgid "Waste Basket"
msgstr "Papelera"

#: map-quick/osmelementinformationmodel_data.cpp:175
msgctxt "OSM::amenity/shop"
msgid "Wine"
msgstr "Vinos"

#: map-quick/osmelementinformationmodel_data.cpp:181
msgctxt "OSM::cuisine"
msgid "American"
msgstr "Americana"

#: map-quick/osmelementinformationmodel_data.cpp:182
msgctxt "OSM::cuisine"
msgid "Arab"
msgstr "Árabe"

#: map-quick/osmelementinformationmodel_data.cpp:183
msgctxt "OSM::cuisine"
msgid "Argentinian"
msgstr "Argentina"

#: map-quick/osmelementinformationmodel_data.cpp:184
msgctxt "OSM::cuisine"
msgid "Asian"
msgstr "Asiática"

#: map-quick/osmelementinformationmodel_data.cpp:185
msgctxt "OSM::cuisine"
msgid "Austrian"
msgstr "Austríaca"

#: map-quick/osmelementinformationmodel_data.cpp:186
#: map-quick/osmelementinformationmodel_data.cpp:187
msgctxt "OSM::cuisine"
msgid "BBQ"
msgstr "Barbacoa"

#: map-quick/osmelementinformationmodel_data.cpp:188
msgctxt "OSM::cuisine"
msgid "Brazilian"
msgstr "Brasileña"

#: map-quick/osmelementinformationmodel_data.cpp:189
msgctxt "OSM::cuisine"
msgid "Breakfast"
msgstr "Desayuno"

#: map-quick/osmelementinformationmodel_data.cpp:190
msgctxt "OSM::cuisine"
msgid "Burger"
msgstr "Hamburguesas"

#: map-quick/osmelementinformationmodel_data.cpp:191
msgctxt "OSM::cuisine"
msgid "Cake"
msgstr "Tartas"

#: map-quick/osmelementinformationmodel_data.cpp:192
msgctxt "OSM::cuisine"
msgid "Chicken"
msgstr "Pollo"

#: map-quick/osmelementinformationmodel_data.cpp:193
msgctxt "OSM::cuisine"
msgid "Chinese"
msgstr "China"

#: map-quick/osmelementinformationmodel_data.cpp:194
msgctxt "OSM::cuisine"
msgid "Coffee Shop"
msgstr "Cafetería"

#: map-quick/osmelementinformationmodel_data.cpp:195
msgctxt "OSM::cuisine"
msgid "Cookies"
msgstr "Galletas"

#: map-quick/osmelementinformationmodel_data.cpp:196
msgctxt "OSM::cuisine"
msgid "Crêpe"
msgstr "Crêpe"

#: map-quick/osmelementinformationmodel_data.cpp:197
msgctxt "OSM::cuisine"
msgid "Donut"
msgstr "Donut"

#: map-quick/osmelementinformationmodel_data.cpp:198
msgctxt "OSM::cuisine"
msgid "Falafel"
msgstr "Falafel"

#: map-quick/osmelementinformationmodel_data.cpp:199
msgctxt "OSM::cuisine"
msgid "Fish"
msgstr "Pescado"

#: map-quick/osmelementinformationmodel_data.cpp:200
msgctxt "OSM::cuisine"
msgid "Fish & Chips"
msgstr "Fish & Chips"

#: map-quick/osmelementinformationmodel_data.cpp:201
msgctxt "OSM::cuisine"
msgid "French"
msgstr "Francesa"

#: map-quick/osmelementinformationmodel_data.cpp:202
msgctxt "OSM::cuisine"
msgid "German"
msgstr "Alemana"

#: map-quick/osmelementinformationmodel_data.cpp:203
msgctxt "OSM::cuisine"
msgid "Greek"
msgstr "Griega"

#: map-quick/osmelementinformationmodel_data.cpp:204
msgctxt "OSM::cuisine"
msgid "Ice Cream"
msgstr "Helados"

#: map-quick/osmelementinformationmodel_data.cpp:205
msgctxt "OSM::cuisine"
msgid "Indian"
msgstr "India"

#: map-quick/osmelementinformationmodel_data.cpp:206
msgctxt "OSM::cuisine"
msgid "Indonesian"
msgstr "Indonesia"

#: map-quick/osmelementinformationmodel_data.cpp:207
msgctxt "OSM::cuisine"
msgid "International"
msgstr "Internacional"

#: map-quick/osmelementinformationmodel_data.cpp:208
msgctxt "OSM::cuisine"
msgid "Italian"
msgstr "Italiana"

#: map-quick/osmelementinformationmodel_data.cpp:209
#: map-quick/osmelementinformationmodel_data.cpp:222
msgctxt "OSM::cuisine"
msgid "Pizza"
msgstr "Pizza"

#: map-quick/osmelementinformationmodel_data.cpp:210
msgctxt "OSM::cuisine"
msgid "Japanese"
msgstr "Japonesa"

#: map-quick/osmelementinformationmodel_data.cpp:211
msgctxt "OSM::cuisine"
msgid "Juice"
msgstr "Zumos"

#: map-quick/osmelementinformationmodel_data.cpp:212
msgctxt "OSM::cuisine"
msgid "Kebab"
msgstr "Kebab"

#: map-quick/osmelementinformationmodel_data.cpp:213
msgctxt "OSM::cuisine"
msgid "Korean"
msgstr "Coreana"

#: map-quick/osmelementinformationmodel_data.cpp:214
msgctxt "OSM::cuisine"
msgid "Lebanese"
msgstr "Libanesa"

#: map-quick/osmelementinformationmodel_data.cpp:215
msgctxt "OSM::cuisine"
msgid "Local"
msgstr "Local"

#: map-quick/osmelementinformationmodel_data.cpp:216
msgctxt "OSM::cuisine"
msgid "Mediterranean"
msgstr "Mediterránea"

#: map-quick/osmelementinformationmodel_data.cpp:217
msgctxt "OSM::cuisine"
msgid "Mexican"
msgstr "Mejicana"

#: map-quick/osmelementinformationmodel_data.cpp:218
msgctxt "OSM::cuisine"
msgid "Noodle"
msgstr "Fideos"

#: map-quick/osmelementinformationmodel_data.cpp:219
msgctxt "OSM::cuisine"
msgid "Pakistani"
msgstr "Pakistaní"

#: map-quick/osmelementinformationmodel_data.cpp:220
msgctxt "OSM::cuisine"
msgid "Pancake"
msgstr "Tortitas"

#: map-quick/osmelementinformationmodel_data.cpp:221
msgctxt "OSM::cuisine"
msgid "Pasta"
msgstr "Pasta"

#: map-quick/osmelementinformationmodel_data.cpp:223
msgctxt "OSM::cuisine"
msgid "Polish"
msgstr "Polaca"

#: map-quick/osmelementinformationmodel_data.cpp:224
msgctxt "OSM::cuisine"
msgid "Portuguese"
msgstr "Portuguesa"

#: map-quick/osmelementinformationmodel_data.cpp:225
msgctxt "OSM::cuisine"
msgid "Ramen"
msgstr "Ramen"

#: map-quick/osmelementinformationmodel_data.cpp:226
msgctxt "OSM::cuisine"
msgid "Regional"
msgstr "Regional"

#: map-quick/osmelementinformationmodel_data.cpp:227
msgctxt "OSM::cuisine"
msgid "Salad"
msgstr "Ensaladas"

#: map-quick/osmelementinformationmodel_data.cpp:228
msgctxt "OSM::cuisine"
msgid "Sandwich"
msgstr "Sándwiches"

#: map-quick/osmelementinformationmodel_data.cpp:229
msgctxt "OSM::cuisine"
msgid "Sausage"
msgstr "Salchichas"

#: map-quick/osmelementinformationmodel_data.cpp:230
msgctxt "OSM::cuisine"
msgid "Seafood"
msgstr "Marisco"

#: map-quick/osmelementinformationmodel_data.cpp:231
msgctxt "OSM::cuisine"
msgid "Soup"
msgstr "Sopas"

#: map-quick/osmelementinformationmodel_data.cpp:232
msgctxt "OSM::cuisine"
msgid "Spanish"
msgstr "Española"

#: map-quick/osmelementinformationmodel_data.cpp:233
#: map-quick/osmelementinformationmodel_data.cpp:234
msgctxt "OSM::cuisine"
msgid "Steak"
msgstr "Carnes"

#: map-quick/osmelementinformationmodel_data.cpp:235
msgctxt "OSM::cuisine"
msgid "Sushi"
msgstr "Sushi"

#: map-quick/osmelementinformationmodel_data.cpp:236
msgctxt "OSM::cuisine"
msgid "Tapas"
msgstr "Tapas"

#: map-quick/osmelementinformationmodel_data.cpp:237
msgctxt "OSM::cuisine"
msgid "Thai"
msgstr "Tailandesa"

#: map-quick/osmelementinformationmodel_data.cpp:238
msgctxt "OSM::cuisine"
msgid "Turkish"
msgstr "Turca"

#: map-quick/osmelementinformationmodel_data.cpp:239
msgctxt "OSM::cuisine"
msgid "Vegetarian"
msgstr "Vegetariana"

#: map-quick/osmelementinformationmodel_data.cpp:240
msgctxt "OSM::cuisine"
msgid "Vietnamese"
msgstr "Vietnamita"

#: map-quick/osmelementinformationmodel_data.cpp:252
msgctxt "OSM::diet_type"
msgid "gluten free"
msgstr "sin gluten"

#: map-quick/osmelementinformationmodel_data.cpp:253
msgctxt "OSM::diet_type"
msgid "halal"
msgstr "halal"

#: map-quick/osmelementinformationmodel_data.cpp:254
msgctxt "OSM::diet_type"
msgid "kosher"
msgstr "kosher"

#: map-quick/osmelementinformationmodel_data.cpp:255
msgctxt "OSM::diet_type"
msgid "lactose free"
msgstr "sin lactosa"

#: map-quick/osmelementinformationmodel_data.cpp:256
msgctxt "OSM::diet_type"
msgid "vegan"
msgstr "vegana"

#: map-quick/osmelementinformationmodel_data.cpp:257
msgctxt "OSM::diet_type"
msgid "vegetarian"
msgstr "vegetariana"

#: map-quick/osmelementinformationmodel_data.cpp:285
msgctxt "OSM::payment_method"
msgid "American Express"
msgstr "American Express"

#: map-quick/osmelementinformationmodel_data.cpp:286
msgctxt "OSM::payment_method"
msgid "Apple Pay"
msgstr "Apple Pay"

#: map-quick/osmelementinformationmodel_data.cpp:287
msgctxt "OSM::payment_method"
msgid "Diners Club"
msgstr "Diners Club"

#: map-quick/osmelementinformationmodel_data.cpp:288
msgctxt "OSM::payment_method"
msgid "Discover Card"
msgstr "Discover Card"

#: map-quick/osmelementinformationmodel_data.cpp:289
msgctxt "OSM::payment_method"
msgid "JCB"
msgstr "JCB"

#: map-quick/osmelementinformationmodel_data.cpp:290
msgctxt "OSM::payment_method"
msgid "Girocard"
msgstr "Girocard"

#: map-quick/osmelementinformationmodel_data.cpp:291
msgctxt "OSM::payment_method"
msgid "Google Pay"
msgstr "Google Pay"

#: map-quick/osmelementinformationmodel_data.cpp:292
msgctxt "OSM::payment_method"
msgid "Maestro"
msgstr "Maestro"

#: map-quick/osmelementinformationmodel_data.cpp:293
msgctxt "OSM::payment_method"
msgid "Mastercard"
msgstr "Mastercard"

#: map-quick/osmelementinformationmodel_data.cpp:294
msgctxt "OSM::payment_method"
msgid "UnionPay"
msgstr "UnionPay"

#: map-quick/osmelementinformationmodel_data.cpp:295
#: map-quick/osmelementinformationmodel_data.cpp:296
msgctxt "OSM::payment_method"
msgid "V Pay"
msgstr "V Pay"

#: map-quick/osmelementinformationmodel_data.cpp:297
msgctxt "OSM::payment_method"
msgid "Visa"
msgstr "Visa"

#: map-quick/osmelementinformationmodel_data.cpp:301
msgctxt "OSM::wheelchair_access"
msgid "limited"
msgstr "limitado"

#: map-quick/osmelementinformationmodel_data.cpp:302
msgctxt "OSM::wheelchair_access"
msgid "no"
msgstr "no"

#: map-quick/osmelementinformationmodel_data.cpp:303
msgctxt "OSM::wheelchair_access"
msgid "yes"
msgstr "sí"

#: map-quick/osmelementinformationmodel_data.cpp:315
msgctxt "OSM::charging_station_socket"
msgid "Chademo"
msgstr "Chademo"

#: map-quick/osmelementinformationmodel_data.cpp:316
msgctxt "OSM::charging_station_socket"
msgid "Schuko"
msgstr "Schuko"

#: map-quick/osmelementinformationmodel_data.cpp:317
msgctxt "OSM::charging_station_socket"
msgid "Tesla"
msgstr "Tesla"

#: map-quick/osmelementinformationmodel_data.cpp:318
msgctxt "OSM::charging_station_socket"
msgid "Tesla Supercharger"
msgstr "Tesla Supercharger"

#: map-quick/osmelementinformationmodel_data.cpp:319
msgctxt "OSM::charging_station_socket"
msgid "Tesla Supercharger CCS"
msgstr "Tesla Supercharger CCS"

#: map-quick/osmelementinformationmodel_data.cpp:320
msgctxt "OSM::charging_station_socket"
msgid "Type 2"
msgstr "Tipo 2"

#: map-quick/osmelementinformationmodel_data.cpp:321
msgctxt "OSM::charging_station_socket"
msgid "Type 2 cable"
msgstr "Cable de tipo 2"

#: map-quick/osmelementinformationmodel_data.cpp:322
msgctxt "OSM::charging_station_socket"
msgid "Type 2 CCS"
msgstr "Tipo 2 CCS"

#: map-quick/osmelementinformationmodel_data.cpp:323
msgctxt "OSM::charging_station_socket"
msgid "Type E"
msgstr "Tipo E"

#: map-quick/osmelementinformationmodel_data.cpp:335
msgctxt "OSM::charging_station_authentication"
msgid "app"
msgstr "app"

#: map-quick/osmelementinformationmodel_data.cpp:336
msgctxt "OSM::charging_station_authentication"
msgid "membership card"
msgstr "tarjeta de socio"

#: map-quick/osmelementinformationmodel_data.cpp:337
msgctxt "OSM::charging_station_authentication"
msgid "NFC"
msgstr "NFC"

#: map-quick/osmelementinformationmodel_data.cpp:338
msgctxt "OSM::charging_station_authentication"
msgid "none"
msgstr "ninguna"

#: map-quick/osmelementinformationmodel_data.cpp:339
msgctxt "OSM::charging_station_authentication"
msgid "phone call"
msgstr "llamada telefónica"

#: map-quick/osmelementinformationmodel_data.cpp:340
msgctxt "OSM::charging_station_authentication"
msgid "SMS"
msgstr "SMS"

#: map-quick/osmelementinformationmodel_data.cpp:347
msgctxt "OSM::bicycle_parking"
msgid "anchors"
msgstr "anclajes"

#: map-quick/osmelementinformationmodel_data.cpp:348
msgctxt "OSM::bicycle_parking"
msgid "bollard"
msgstr "bolardo"

#: map-quick/osmelementinformationmodel_data.cpp:349
msgctxt "OSM::bicycle_parking"
msgid "building"
msgstr "edificio"

#: map-quick/osmelementinformationmodel_data.cpp:350
msgctxt "OSM::bicycle_parking"
msgid "ground slots"
msgstr "ranuras en suelo"

#: map-quick/osmelementinformationmodel_data.cpp:351
msgctxt "OSM::bicycle_parking"
msgid "lockers"
msgstr "casilleros"

#: map-quick/osmelementinformationmodel_data.cpp:352
msgctxt "OSM::bicycle_parking"
msgid "racks"
msgstr "bastidores"

#: map-quick/osmelementinformationmodel_data.cpp:353
msgctxt "OSM::bicycle_parking"
msgid "shed"
msgstr "cobertizo"

#: map-quick/osmelementinformationmodel_data.cpp:354
msgctxt "OSM::bicycle_parking"
msgid "stands"
msgstr "soportes"

#: map-quick/osmelementinformationmodel_data.cpp:355
msgctxt "OSM::bicycle_parking"
msgid "wall loops"
msgstr "ganchos de pared"

#: map-quick/osmelementinformationmodel_data.cpp:356
msgctxt "OSM::bicycle_parking"
msgid "wide stands"
msgstr "anclajes"

#: map-quick/osmelementinformationmodel_data.cpp:363
#, kde-format
msgctxt "available rental vehicles"
msgid "%1 bike"
msgid_plural "%1 bikes"
msgstr[0] "%1 bicicleta"
msgstr[1] "%1 bicicletas"

#: map-quick/osmelementinformationmodel_data.cpp:364
#, kde-format
msgctxt "available rental vehicles"
msgid "%1 pedelec"
msgid_plural "%1 pedelecs"
msgstr[0] "%1 bicilec"
msgstr[1] "%1 bicilecs"

#: map-quick/osmelementinformationmodel_data.cpp:365
#, kde-format
msgctxt "available rental vehicles"
msgid "%1 kick scooter"
msgid_plural "%1 kick scooters"
msgstr[0] "%1 patinete"
msgstr[1] "%1 patinetes"

#: map-quick/osmelementinformationmodel_data.cpp:366
#, kde-format
msgctxt "available rental vehicles"
msgid "%1 moped"
msgid_plural "%1 mopeds"
msgstr[0] "%1 ciclomotor"
msgstr[1] "%1 ciclomotores"

#: map-quick/osmelementinformationmodel_data.cpp:367
#, kde-format
msgctxt "available rental vehicles"
msgid "%1 car"
msgid_plural "%1 cars"
msgstr[0] "%1 automóvil"
msgstr[1] "%1 automóviles"

#: map-quick/osmelementinformationmodel_data.cpp:378
msgctxt "OSM::gender_segregation"
msgid "female"
msgstr "mujer"

#: map-quick/osmelementinformationmodel_data.cpp:379
msgctxt "OSM::gender_segregation"
msgid "male"
msgstr "hombre"

#: map-quick/osmelementinformationmodel_data.cpp:380
msgctxt "OSM::gender_segregation"
msgid "unisex"
msgstr "unisex"

#: map-quick/osmelementinformationmodel_data.cpp:392
msgctxt "tactile writing"
msgid "braille"
msgstr "braille"

#: map-quick/osmelementinformationmodel_data.cpp:393
msgctxt "tactile writing"
msgid "embossed printed letters"
msgstr "letras impresas repujadas"

#: map-quick/osmelementinformationmodel_data.cpp:394
msgctxt "tactile writing"
msgid "engraved printed letters"
msgstr "letras impresas grabadas"
